/**
 * Created at 19.09.2010
 */
package com.yabby.storybook.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.yabby.storybook.EditBookActivity;
import com.yabby.storybook.R;
import com.yabby.storybook.data.BooksLoader;
import com.yabby.storybook.model.Book;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class SaveBookDialog extends Dialog implements OnClickListener {
	
	private final Book book;
	
	private final EditBookActivity activity;
	
	public SaveBookDialog(EditBookActivity activity, Book book) {
		super(activity);
		this.book = book;
		this.activity = activity;
		setContentView(R.layout.save_book_dialog);
		findViewById(R.id.save_button).setOnClickListener(this);
		findViewById(R.id.cancel_button).setOnClickListener(this);
		((TextView)findViewById(R.id.bookNameEdit)).setText(book.getName() + ".book");
		setTitle(R.string.save_book_dialog_title);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.cancel_button:
				hide();
				break;
			case R.id.save_button:
				EditText nameField = (EditText) findViewById(R.id.bookNameEdit);
				final String bookName = nameField.getText().toString();
				final ProgressDialog dialog = ProgressDialog.show(getContext(), "", 
	                    "Saving. Please wait...", true);
				new Thread(){
					@Override
					public void run() {
						BooksLoader.saveBook(getContext(), book, bookName);
						activity.getIntent().putExtra(EditBookActivity.FILE_PATH, book.getPath());
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								dialog.dismiss();
								hide();
								activity.finish();
							}
						});
					};
				}.start();
				break;
			default:
				break;
		}
	}

}
