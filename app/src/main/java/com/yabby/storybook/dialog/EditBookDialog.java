/**
 * Created at 12.12.2010
 */
package com.yabby.storybook.dialog;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.yabby.storybook.R;
import com.yabby.storybook.adapter.BookAdapter;
import com.yabby.storybook.data.BooksLoader;
import com.yabby.storybook.model.BookCover;

import java.util.List;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class EditBookDialog extends Dialog implements OnClickListener {

	private BookCover mBook;
	private final BookAdapter mBookAdapter;
	private final Activity mActivity;
	
	public void setBookForEdit(BookCover book){
		this.mBook = book;
		EditText nameField = (EditText) findViewById(R.id.bookNameEdit);
		nameField.setText(book.getBookName());
	}
	
	public EditBookDialog(Activity context, BookAdapter adapter) {
		super(context);
		mBookAdapter = adapter;
		mActivity = context;
		setContentView(R.layout.edit_book_dialog);
		findViewById(R.id.save_button).setOnClickListener(this);
		findViewById(R.id.delete_button).setOnClickListener(this);
		findViewById(R.id.cancel_button).setOnClickListener(this);
		setTitle(R.string.edit_book_dialog_title);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.cancel_button:
				hide();
				break;
			case R.id.delete_button:
				AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
				builder.setMessage(getContext().getResources().getString(R.string.delete_book_warning, mBook.getBookName()))
				       .setCancelable(false)
				       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	   BooksLoader.deleteBook(getContext(), mBook);
				        	   mBookAdapter.deleteBook(mBook);
				        	   hide();
				           }
				       })
				       .setNegativeButton("No", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				           }
				       });
				AlertDialog alert = builder.create();
				alert.show();
				break;
			case R.id.save_button:
				EditText nameField = (EditText) findViewById(R.id.bookNameEdit);
				String bookName = nameField.getText().toString().trim();
				if(!mBook.getBookName().equals(bookName)){
					List<BookCover> covers = BooksLoader.loadCovers(getContext());
					for (BookCover bookCover : covers) {
						if(bookCover.getBookName().equals(bookName)){
							Toast.makeText(getContext(), R.string.book_exist, Toast.LENGTH_LONG).show();
							return;
						}
					}
					BooksLoader.renameBook(mActivity, mBook, bookName);
					mBookAdapter.changeName(mBook, bookName);
					hide();
				}
				break;
			default:
				break;
		}
	}

}
