/**
 * Created at 19.09.2010
 */
package com.yabby.storybook.dialog;

import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;

import com.yabby.storybook.EditBookActivity;
import com.yabby.storybook.R;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class InsertPageDialog extends Dialog implements OnClickListener {

	private int position;
	
	private final EditBookActivity editBookActivity;
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	public InsertPageDialog(EditBookActivity editBookActivity) {
		super(editBookActivity);
		this.editBookActivity = editBookActivity;
		setContentView(R.layout.insert_page_dialog);
		setTitle(R.string.insert_page_dialog_title);
		findViewById(R.id.insert_before).setOnClickListener(this);
		findViewById(R.id.insert_after).setOnClickListener(this);
		findViewById(R.id.cancel).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.insert_before:
				editBookActivity.addPageResult(position);
				dismiss();
				break;
			case R.id.insert_after:
				editBookActivity.addPageResult(position + 1);
				dismiss();
				break;
			case R.id.cancel:
				dismiss();
				break;
			default:
				break;
		}
	}
}