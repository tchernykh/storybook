/**
 * Created at 19.09.2010
 */
package com.yabby.storybook.dialog;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.yabby.storybook.R;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.Page;
import com.yabby.storybook.tools.AudioRecorder;

import java.io.File;
import java.io.IOException;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class CaptureSoundDialog extends Dialog implements OnClickListener {

    private static final String LOG_TAG = CaptureSoundDialog.class.getSimpleName();

	public interface CaptureSoundDialogHandler {
		void setRecordedSoundValue(String name);
	}

	private AudioRecorder ar = null;
	private String fileName;
	private final CaptureSoundDialogHandler handler;
	private Book book;
	private Page page;

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public void setPage(Page page) {
		this.page = page;
		ImageView contentImage = (ImageView) findViewById(R.id.sound_record_image);
		if (page.getImage() != null) {
			contentImage.setImageURI(Uri.parse(book.getPath() + "/" + page.getImage()));
		}
	}

	public CaptureSoundDialog(Context context, CaptureSoundDialogHandler handler) {
		super(context);
		this.handler = handler;
		setContentView(R.layout.capture_sound_dialog);
		setTitle(R.string.record_media_dialog_title);
		findViewById(R.id.start_button).setOnClickListener(this);
		findViewById(R.id.cancel_button).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.start_button:
				if (ar == null) {
					((Button) findViewById(R.id.start_button)).setText(R.string.stop);
					ImageView contentImage = (ImageView) findViewById(R.id.sound_record_image);
					if (page.getImage() != null) {
						contentImage.setImageURI(Uri.parse(book.getPath() + "/" + page.getImage()));
					} else {
						contentImage.setImageResource(R.drawable.sound_recorder);
					}
					File sndFile = new File(book.getPath(), fileName);
					if (sndFile.exists()) {
						sndFile.delete();
					}
					ar = new AudioRecorder(sndFile.getAbsolutePath());
					try {
						ar.start();
					} catch (IOException e) {
						Log.e(LOG_TAG, "start", e);
					}
				} else {
					((Button) findViewById(R.id.start_button)).setText(R.string.start);
					ImageView contentImage = (ImageView) findViewById(R.id.sound_record_image);
					if (page.getImage() != null) {
						contentImage.setImageURI(Uri.parse(book.getPath() + "/" + page.getImage()));
					} else {
						contentImage.setImageResource(R.drawable.sound_recorder_inactive);
					}
					try {
						ar.stop();
					} catch (IOException e) {
						Log.e(LOG_TAG, "stop", e);
					}
					ar = null;
					hide();
					handler.setRecordedSoundValue(fileName);
				}
				break;
			case R.id.cancel_button:
				dismiss();
				break;
		}
	}

}
