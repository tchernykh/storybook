/**
 * Created at 19.09.2010
 */
package com.yabby.storybook.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

import com.yabby.storybook.R;
import com.yabby.storybook.Utils;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class InfoDialog extends Dialog implements OnClickListener {

	public static final String NO_REPEAT_PREF_NAME = "NO_REPEAT";

	public InfoDialog(Context context) {
		super(context);
		setContentView(R.layout.info_dialog);
		setTitle(R.string.info_dialog_title);
		findViewById(R.id.continue_button).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(!((CheckBox)findViewById(R.id.repeatCheckbox)).isChecked()){
			getContext().getSharedPreferences(Utils.PREFERENCES_NAME, Context.MODE_PRIVATE).edit().putBoolean(NO_REPEAT_PREF_NAME, true).commit();			
		}
		hide();
	}

}
