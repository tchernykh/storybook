/**
 * Created at 19.09.2010
 */
package com.yabby.storybook.dialog;

import android.app.Dialog;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

import com.yabby.storybook.EditBookActivity;
import com.yabby.storybook.R;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class EditImageDialog extends Dialog implements OnClickListener {

	private final EditBookActivity activity;

	public EditImageDialog(EditBookActivity activity) {
		super(activity);
		this.activity = activity;
		getWindow().addFlags(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.edit_image_dialog);
		findViewById(R.id.change_button).setOnClickListener(this);
		findViewById(R.id.cancel_button).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.change_button:
				activity.changeImage();
				dismiss();
				break;
			case R.id.cancel_button:
				dismiss();
				break;
		}
	}

	public void showImageUrl(String imagePath) {
		ImageView contentImage = (ImageView) findViewById(R.id.image);
		contentImage.setImageURI(Uri.parse(imagePath));
	}

}
