/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.dialog;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.yabby.storybook.EditBookActivity;
import com.yabby.storybook.R;
import com.yabby.storybook.data.BooksLoader;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.BookCover;
import com.yabby.storybook.model.Page;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class CreateNewBookDialog extends Dialog implements OnClickListener {

	public CreateNewBookDialog(Context context) {
		super(context);
		setContentView(R.layout.create_new_book_dialog);
		findViewById(R.id.create_button).setOnClickListener(this);
		findViewById(R.id.cancel_button).setOnClickListener(this);
		setTitle(R.string.new_book_dialog_title);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.cancel_button:
				hide();
				break;
			case R.id.create_button:
				EditText nameField = (EditText) findViewById(R.id.bookNameEdit);
				String bookName = nameField.getText().toString().trim();
				List<BookCover> covers = BooksLoader.loadCovers(getContext());
				for (BookCover bookCover : covers) {
					if(bookCover.getBookName().equals(bookName)){
						Toast.makeText(getContext(), R.string.book_exist, Toast.LENGTH_LONG).show();
						return;
					}
				}
				Intent editNewBookIntent = new Intent(getContext(), EditBookActivity.class);
				Book newBook = new Book();
				ArrayList<Page> content = new ArrayList<Page>();
				newBook.setContent(content);
				newBook.setName(bookName);
				File dir = new File(getContext().getFilesDir(), newBook.getName());
				newBook.setPath(dir.getAbsolutePath());
				dir.mkdirs();
				editNewBookIntent.putExtra(EditBookActivity.BOOK_PARAM, newBook);
				nameField.setText("");
				hide();
				getContext().startActivity(editNewBookIntent);
				break;
			default:
				break;
		}
	}

}
