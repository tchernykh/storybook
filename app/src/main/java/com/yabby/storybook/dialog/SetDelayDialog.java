/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.dialog;


import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.yabby.storybook.R;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class SetDelayDialog extends Dialog implements OnClickListener {

	public interface SetDelayDialogHandler{
		void setDelayValues(int delay);
	}
	
	private final SetDelayDialogHandler handler;
	
	public SetDelayDialog(Context context, SetDelayDialogHandler handler) {
		super(context);
		this.handler = handler;
		setContentView(R.layout.set_delay_dialog);
		findViewById(R.id.set_button).setOnClickListener(this);
		findViewById(R.id.cancel_button).setOnClickListener(this);
		setTitle(R.string.enter_delay_dialog_title);
		((SeekBar)findViewById(R.id.DelayBar)).setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				((TextView)findViewById(R.id.DelayValueLabel)).setText(""+progress+getContext().getString(R.string.sec));
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.cancel_button:
				hide();
				break;
			case R.id.set_button:
				handler.setDelayValues(((SeekBar)findViewById(R.id.DelayBar)).getProgress());
				hide();
				break;
			default:
				break;
		}
	}

	public void setValue(int delaySec) {
		((SeekBar)findViewById(R.id.DelayBar)).setProgress(delaySec);
	}

}
