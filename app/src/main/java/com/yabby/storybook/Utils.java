/**
 * Created at 09.10.2010
 */
package com.yabby.storybook;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class Utils {

    private static final String LOG_TAG = Utils.class.getSimpleName();

	public static final String PREFERENCES_NAME = "StoreBook";

	public static final String APP_FOLDER_PATH_PARAM_NAME = "AppFolderPath";

	private static final String APP_FOLDER = "StoryBook";

	public static File getAppDir(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		String path = prefs.getString(APP_FOLDER_PATH_PARAM_NAME, null);
		File appDir = null;
		if (path == null) {
			appDir = new File(Environment.getExternalStorageDirectory(), APP_FOLDER);
            Log.i(LOG_TAG, "getAppDir: " + appDir);

			if (!appDir.exists()) {
				appDir.mkdirs();
			}
			path = appDir.getAbsolutePath();
			prefs.edit().putString(APP_FOLDER_PATH_PARAM_NAME, path).commit();
		} else {
			appDir = new File(path);
			// TODO check if our folder is still exists
		}
		return appDir;
	}

	public static void copyStream(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[4096];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
	}

	public static void safeClose(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}

	public static void safeClose(OutputStream os) {
		if (os != null) {
			try {
				os.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}

    public static int dipToPx(Context context, int dip) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dip * scale + 0.5f);
    }
}
