/**
 * Created at 26.09.2010
 */
package com.yabby.storybook;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.yabby.storybook.data.BooksLoader;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.Page;
import com.yabby.storybook.ui.BookSurfaceView;
import com.yabby.storybook.ui.BookSurfaceView.BitmapProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class PlayActivity extends Activity implements OnClickListener {

	public static final String BOOK_PARAM = "book";

	public static final String PAGE_PARAM = "page";
    public static final String LOG_TAG = "PlayActivity";

    private int mThumbWidth;

	private int mThumbHeight;

	private BookSurfaceView mBookSurfaceView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(com.yabby.storybook.R.layout.play);

		mBookSurfaceView = (BookSurfaceView) findViewById(com.yabby.storybook.R.id.book_view);
		mThumbWidth = getResources().getDisplayMetrics().widthPixels;
		mThumbHeight = getResources().getDisplayMetrics().heightPixels;
		int expHeight = mThumbWidth * 3 / 4;
		if (mThumbHeight > expHeight) {
			mThumbHeight = expHeight;
		}
		mBookSurfaceView.setLayoutParams(new RelativeLayout.LayoutParams(mThumbWidth, mThumbHeight));
		Book book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		BooksLoader.loadBookPagesData(this, book);
		Integer page = (Integer) getIntent().getSerializableExtra(PAGE_PARAM);
		mBookSurfaceView.setBitmapProvider(new SelectedImagesProvider(book));
		if (page != null) {
			mBookSurfaceView.setCurrentIndex(page);
		}
		mBookSurfaceView.init(mThumbWidth, mThumbHeight);

		mBookSurfaceView.setOnClickListener(this);
	}

	class SelectedImagesProvider implements BitmapProvider {

		private final Book mBook;
		
		private final Bitmap[] images;

		public SelectedImagesProvider(Book book) {
			mBook = book;
			images = new Bitmap[book.getContent().size()];
			int i = 0;
			byte[] buf = new byte[16 * 1024];
			for (Page page : book.getContent()) {
				BitmapFactory.Options opts = new BitmapFactory.Options();
				int w = mThumbWidth;
				int h = mThumbHeight;
				opts.outWidth = w;
				opts.outHeight = h;

				if (mThumbWidth != 0 && mThumbHeight != 0) {
					int width_tmp = page.getImageWidth(), height_tmp = page.getImageHeight();
					int imgScale = 1;
					while (true) {
						if (width_tmp / 2 < w || height_tmp / 2 < h) {
							break;
						}
						width_tmp /= 2;
						height_tmp /= 2;
						imgScale++;
					}
					opts.inSampleSize = imgScale;
				} else {
					opts.inSampleSize = 8;
				}

				opts.inTempStorage = buf;
				Bitmap res = BitmapFactory.decodeFile(mBook.getPath() + "/" + page.getImage(), opts);
				Bitmap result = Bitmap.createScaledBitmap(res, mThumbWidth, mThumbHeight, true);
				images[i] = result;
				res.recycle();
				i++;
			}
		}

		@Override
		public Bitmap getBitmap(int index) {
			return images[index];

		}

		@Override
		public int getBitmapsCount() {
			return mBook.getContent().size();
		}

	}

	private MediaPlayer mp;

	@Override
	public void onClick(View v) {
		if (mp != null) {
			return;
		}
		Book book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		Page page = book.getContent().get(mBookSurfaceView.getCurrentIndex());
		if (page.getMedia() != null && page.getMedia().length() > 0) {
			try {
				File file = new File(book.getPath() + "/" + page.getMedia());
				final FileInputStream fis = new FileInputStream(file);

				mp = new MediaPlayer();
				mp.setDataSource(fis.getFD());
				mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mp.prepare();
				mp.start();
				mp.setOnCompletionListener(new OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer paramMediaPlayer) {
						try {
							fis.close();
							mp = null;
						} catch (IOException e) {
							Log.e(LOG_TAG, "close stream", e);
						}
					}
				});
			} catch (Exception ex) {
				Log.e(LOG_TAG, "playing media", ex);
			}
		}
	}

}
