/**
 * Created at 13.09.2010
 */
package com.yabby.storybook;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.yabby.storybook.adapter.BookAdapter;
import com.yabby.storybook.data.BooksLoader;
import com.yabby.storybook.dialog.CreateNewBookDialog;
import com.yabby.storybook.dialog.EditBookDialog;
import com.yabby.storybook.dialog.InfoDialog;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.BookCover;

import java.util.List;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class SelectBookActivity extends Activity {

    private static final String LOG_TAG = SelectBookActivity.class.getSimpleName();

    private static final int CREATE_NEW_BOOK_DIALOG = 0;
	private static final int INFO_DIALOG = 1;
	private static final int EDIT_BOOK_DIALOG = 2;

	private BookAdapter mBookAdapter;

	private boolean mEditMode = false;

	private BookCover mBookForEdit = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.yabby.storybook.R.layout.select_book);
		GridView booksGrid = (GridView) findViewById(com.yabby.storybook.R.id.BookView);
		booksGrid.setCacheColorHint(0);
		mBookAdapter = new BookAdapter(this);
		booksGrid.setAdapter(mBookAdapter);
		booksGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				if (mEditMode) {
					mBookForEdit = mBookAdapter.getItem(position);
					showDialog(EDIT_BOOK_DIALOG);
				} else {
					final BookCover cover = mBookAdapter.getItem(position);

                    final ProgressDialog dialog = ProgressDialog.show(SelectBookActivity.this, "", getString(com.yabby.storybook.R.string.loading_message),
							true);
					new Thread() {
						@Override
						public void run() {
							final Intent openBookIntent = new Intent(SelectBookActivity.this, EditBookActivity.class);
							Book book = BooksLoader.loadBook(SelectBookActivity.this, cover);
							openBookIntent.putExtra(EditBookActivity.BOOK_PARAM, book);
							openBookIntent.putExtra(EditBookActivity.FILE_PATH, cover.getBookPath());
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									dialog.dismiss();
									startActivity(openBookIntent);
								}
							});
						};
					}.start();
				}
			}
		});
		SharedPreferences prefs = getSharedPreferences(Utils.PREFERENCES_NAME, Context.MODE_PRIVATE);
		boolean noRepeat = prefs.getBoolean(InfoDialog.NO_REPEAT_PREF_NAME, false);
		if (!noRepeat) {
			showDialog(INFO_DIALOG);
		}

		View emptyView = getLayoutInflater().inflate(com.yabby.storybook.R.layout.select_book_empty, null);
		addContentView(emptyView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		booksGrid.setEmptyView(emptyView);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		final ProgressDialog dialog = ProgressDialog.show(SelectBookActivity.this, "", getString(com.yabby.storybook.R.string.books_loading_message), true);
		new Thread() {
			@Override
			public void run() {
				final List<BookCover> booksOnSDCard = BooksLoader.loadCovers(getApplicationContext());
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mBookAdapter.setBooks(booksOnSDCard);
						dialog.dismiss();
					}
				});
			};
		}.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(com.yabby.storybook.R.menu.select_book_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case com.yabby.storybook.R.id.add_menu:
				showDialog(CREATE_NEW_BOOK_DIALOG);
				break;
			case com.yabby.storybook.R.id.edit_menu:
				mEditMode = !mEditMode;
				mBookAdapter.setEditMode(mEditMode);
				break;
		}
		return true;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case CREATE_NEW_BOOK_DIALOG:
				return new CreateNewBookDialog(this);
			case INFO_DIALOG:
				return new InfoDialog(this);
			case EDIT_BOOK_DIALOG:
				return new EditBookDialog(this, mBookAdapter);
			default:
				break;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		switch (id) {
			case EDIT_BOOK_DIALOG:
				((EditBookDialog) dialog).setBookForEdit(mBookForEdit);
				break;
			default:
				break;
		}
	}
}