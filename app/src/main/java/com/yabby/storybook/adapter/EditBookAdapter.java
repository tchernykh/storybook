/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yabby.storybook.R;
import com.yabby.storybook.Utils;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.Page;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class EditBookAdapter extends BaseAdapter implements OnClickListener {

	public interface EditBookHandler {
		void selectImage(int pos);

		void setDelay(int pos);

		void recordMedia(int position);

		void play(int pos);

		void addPage(int pos);
	}

	final LayoutInflater inflater;
	final Book book;
	final EditBookHandler editBookHandler;
	final Context context;

	int thumbWidth;
	int thumbHeight;

	public Book getBook() {
		return book;
	}

	public EditBookAdapter(Context context, EditBookHandler editBookHandler, Book book) {
		this.context = context;
		this.book = book;
		this.editBookHandler = editBookHandler;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		thumbWidth = Utils.dipToPx(context, 60);
		thumbHeight = Utils.dipToPx(context, 45);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return book.getContent().size();
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		long id = getItemId(position);
		if (id > 0) {
			return book.getContent().get((int) id);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		View v;
		if (convertView == null) {
			v = inflater.inflate(R.layout.edit_page_item, parent, false);
		} else {
			v = convertView;
		}
		TextView img = (TextView) v.findViewById(R.id.insertImage);
		TextView rec = (TextView) v.findViewById(R.id.recordMedia);
		TextView play = (TextView) v.findViewById(R.id.play);
		TextView del = (TextView) v.findViewById(R.id.delete);
		TextView delay = (TextView) v.findViewById(R.id.delay);
		LongClickHandler longClickHandler = new LongClickHandler(pos);
		v.setOnLongClickListener(longClickHandler);

		del.setTag("del_" + pos);
		del.setOnClickListener(this);
		del.setOnLongClickListener(longClickHandler);

		img.setTag("img_" + pos);
		img.setOnClickListener(this);
		img.setOnLongClickListener(longClickHandler);

		rec.setTag("rec_" + pos);
		rec.setOnClickListener(this);
		rec.setOnLongClickListener(longClickHandler);

		play.setTag("ply_" + pos);
		play.setOnClickListener(this);
		play.setOnLongClickListener(longClickHandler);

		delay.setTag("pse_" + pos);
		delay.setOnClickListener(this);
		delay.setOnLongClickListener(longClickHandler);

		if (book.getContent().size() >= pos) {
			Page page = book.getContent().get(pos);
			Drawable image;
			if (page.getImage() == null) {
				image = v.getResources().getDrawable(R.drawable.insert_image);
			} else {
				image = new BitmapDrawable(page.getCover());
			}
			image.setBounds(0, 0, thumbWidth, thumbHeight);
			img.setCompoundDrawables(null, image, null, null);
			int delaySec = page.getDelaySec();
			if (delaySec > 0) {
				delay.setText(delaySec + context.getString(R.string.sec));
			} else {
				delay.setText(R.string.delay);
			}
		}
		return v;
	}

	@Override
	public void onClick(View v) {
		String tag = (String) v.getTag();
		if (tag != null) {
			int position = Integer.valueOf(tag.substring(4));
			if (tag.startsWith("del_")) {
				book.getContent().remove(position);
				notifyDataSetChanged();
			} else if (tag.startsWith("img_")) {
				editBookHandler.selectImage(position);
			} else if (tag.startsWith("rec_")) {
				editBookHandler.recordMedia(position);
			} else if (tag.startsWith("ply_")) {
				editBookHandler.play(position);
			} else if (tag.startsWith("pse_")) {
				editBookHandler.setDelay(position);
			}
		}
	}

	class LongClickHandler implements OnLongClickListener {

		int pos;

		public LongClickHandler(int pos) {
			this.pos = pos;
		}

		@Override
		public boolean onLongClick(View v) {
			editBookHandler.addPage(pos);
			return true;
		}

	}
}