/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yabby.storybook.R;
import com.yabby.storybook.model.BookCover;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class BookAdapter extends BaseAdapter {

    private static final String LOG_TAG = BookAdapter.class.getSimpleName();

	private List<BookCover> books;
	private final LayoutInflater inflater;
	private boolean editMode = false;

	public BookAdapter(Context context) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		books = new ArrayList<BookCover>();
	}
	
	public void setBooks(List<BookCover> books) {
		this.books = books;
		this.notifyDataSetChanged();
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return books.size();
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public BookCover getItem(int position) {
		return books.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout v;
		if (convertView == null) {
			v = (RelativeLayout) inflater.inflate(R.layout.book_item, null);
		} else {
			v = (RelativeLayout) convertView;
		}
		TextView bookNameTextView = (TextView) v.findViewById(R.id.book_name);
		ImageView bookCoverImageView = (ImageView) v.findViewById(R.id.book_cover);
		BookCover book = books.get(position);
		bookNameTextView.setText(book.getBookName());
		bookCoverImageView.setImageBitmap(book.getCover());
		v.findViewById(R.id.book_edit).setVisibility(editMode?View.VISIBLE:View.GONE);
        return v;
	}

	public void setEditMode(boolean mEditMode) {
		editMode = mEditMode;
		this.notifyDataSetChanged();
	}

	public void deleteBook(BookCover mBook) {
		books.remove(mBook);
		this.notifyDataSetChanged();
	}

	public void changeName(BookCover mBook, String bookName) {
		BookCover cover = books.get(books.indexOf(mBook));
		cover.setBookName(bookName);
		this.notifyDataSetChanged();
	}

}
