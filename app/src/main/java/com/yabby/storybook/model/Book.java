/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.model;

import android.util.Log;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class Book implements Serializable {

    private static final String LOG_TAG = Book.class.getSimpleName();

	private static final long serialVersionUID = 3087214953888283449L;

	private List<Page> content;

	private String name;

	private Long id;
	
	private String path;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Page> getContent() {
		return content;
	}

	public void setContent(List<Page> content) {
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}

	public String toXml() {
		StringBuilder sb = new StringBuilder("<book id='" + id + "' name='" + name + "'>");
		for (Page page : content) {
			sb.append(page.toXml());
		}
		sb.append("</book>");
		return sb.toString();
	}

	public static Book fromXml(Element element) {
		try {
			Book result = new Book();
			try{
				result.setId(Long.valueOf(element.getAttribute("id")));
			} catch (Exception e) {
                // ignore
			}
			result.setName(element.getAttribute("name"));
			NodeList pageElements = element.getElementsByTagName("page");
			List<Page> pages = new ArrayList<Page>(pageElements.getLength());
			for (int i = 0; i < pageElements.getLength(); i++) {
				pages.add(Page.fromXml((Element) pageElements.item(i)));
			}
			result.setContent(pages);
			return result;
		} catch (Exception ex) {
			Log.e(LOG_TAG, "fromXml", ex);
		}
		return null;
	}
}
