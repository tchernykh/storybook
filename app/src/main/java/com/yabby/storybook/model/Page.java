/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.model;

import android.graphics.Bitmap;
import android.util.Log;

import org.w3c.dom.Element;

import java.io.Serializable;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 *
 */
public class Page implements Serializable{

    private static final String LOG_TAG = Book.class.getSimpleName();

	private static final long serialVersionUID = 5732017732494220490L;
	
	private int delayMillis;
	
	private String image;
	
	private String media;
	
	private int imageWidth;
	
	private int imageHeight; 
	
	private transient Bitmap cover;

	public Bitmap getCover() {
		return cover;
	}
	
	public void setCover(Bitmap cover) {
		this.cover = cover;
	}
	
	public int getDelaySec() {
		return delayMillis;
	}

	public void setDelayMillis(int delayMillis) {
		this.delayMillis = delayMillis;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getMedia() {
		return media;
	}
	
	public void setMedia(String media) {
		this.media = media;
	}
	
	public int getImageHeight() {
		return imageHeight;
	}
	
	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}
	
	public int getImageWidth() {
		return imageWidth;
	}
	
	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}
	
	public String toXml(){
		return "<page delay='"+delayMillis+"' image='"+(image==null?"":image)+"' "+(media==null?"":(" media='"+media+"'"))+" imageHeight='"+imageHeight+"' imageWidth='"+imageWidth+"' />";
	}
	
	public static Page fromXml(Element element) {
		try {
			Page result = new Page();
			result.setDelayMillis(Integer.valueOf(element.getAttribute("delay")));
			result.setImage(element.getAttribute("image"));
			result.setMedia(element.getAttribute("media"));
			String ih = element.getAttribute("imageHeight");
			if(ih!=null && ih.length()>0){
				result.setImageHeight(Integer.valueOf(ih));
			}
			String iw = element.getAttribute("imageWidth");
			if(iw!=null && iw.length()>0){
				result.setImageWidth(Integer.valueOf(iw));
			}
			return result;
		} catch (Exception ex) {
			Log.e(LOG_TAG, "fromXml", ex);
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (Page.class != obj.getClass()) {
			return false;
		}
		Page other = (Page) obj;
		if (image == null) {
			if (other.image != null) {
				return false;
			}
		} else if (!image.equals(other.image)) {
			return false;
		}
		return true;
	}
	
	
}
