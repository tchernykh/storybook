/**
 * Created at 25.09.2010
 */
package com.yabby.storybook.model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class BookCover implements Serializable {

	private static final long serialVersionUID = -3914268409163153487L;

	private String bookName;

	private String bookPath;

	private Bitmap cover;

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookPath() {
		return bookPath;
	}

	public void setBookPath(String bookPath) {
		this.bookPath = bookPath;
	}

	public Bitmap getCover() {
		return cover;
	}

	public void setCover(Bitmap cover) {
		this.cover = cover;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookName == null) ? 0 : bookName.hashCode());
		result = prime * result + ((bookPath == null) ? 0 : bookPath.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (BookCover.class != obj.getClass()) {
			return false;
		}
		BookCover other = (BookCover) obj;
		if (bookName == null) {
			if (other.bookName != null) {
				return false;
			}
		} else if (!bookName.equals(other.bookName)) {
			return false;
		}
		if (bookPath == null) {
			if (other.bookPath != null) {
				return false;
			}
		} else if (!bookPath.equals(other.bookPath)) {
			return false;
		}
		return true;
	}

}