package com.yabby.storybook.image;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import com.yabby.storybook.R;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * The activity can crop specific region of interest from an image.
 */
public class CropImage extends Activity implements OnClickListener {

    private static final String LOG_TAG = CropImage.class.getSimpleName();

	static final int THUMBNAIL_TARGET_SIZE = 320;
	static final int THUMBNAIL_MAX_NUM_PIXELS = 512 * 384;

	// These are various options can be specified in the intent.
	private int mAspectX, mAspectY;
	private boolean mCircleCrop = false;

	// These options specifiy the output image size and whether we should
	// scale the output to fit it (or just crop it).
	private int mOutputX, mOutputY;
	private boolean mScale;
	private boolean mScaleUp = true;

	boolean mWaitingToPick; // Whether we are wait the user to pick a face.
	boolean mSaving; // Whether the "save" button is already clicked.

	private CropImageView mImageView;

	private Bitmap mBitmap;
	HighlightView mCrop;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
        Log.i(LOG_TAG, "onCreate: ");

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.cropimage);

		mImageView = (CropImageView) findViewById(R.id.image);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		if (extras != null) {
			if (extras.getString("circleCrop") != null) {
				mCircleCrop = true;
				mAspectX = 1;
				mAspectY = 1;
			}
			mBitmap = (Bitmap) extras.getParcelable("data");
			mAspectX = extras.getInt("aspectX");
			mAspectY = extras.getInt("aspectY");
			mOutputX = extras.getInt("outputX");
			mOutputY = extras.getInt("outputY");
			mScale = extras.getBoolean("scale", true);
			mScaleUp = extras.getBoolean("scaleUpIfNeeded", true);
		}

		if (mBitmap == null) {
			try {
                String fileName = intent.getStringExtra(Intent.EXTRA_TEXT);
				ParcelFileDescriptor pfd = ParcelFileDescriptor.open(new File(fileName),
						ParcelFileDescriptor.MODE_READ_ONLY);
				mBitmap = Util.makeBitmap(THUMBNAIL_TARGET_SIZE, THUMBNAIL_MAX_NUM_PIXELS, null, null, pfd, null);
			} catch (FileNotFoundException ex) {
				Log.e("CropImage", "init", ex);
			}
		}

		if (mBitmap == null) {
			finish();
			return;
		}

		// Make UI fullscreen.
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		findViewById(R.id.discard).setOnClickListener(this);
		findViewById(R.id.save).setOnClickListener(this);
		findViewById(R.id.rotate_left).setOnClickListener(this);
		findViewById(R.id.rotate_right).setOnClickListener(this);

		initHighlighting();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.save:
				onSaveClicked();
				break;
			case R.id.discard:
				setResult(RESULT_CANCELED);
				finish();
				break;
			case R.id.rotate_left:
				rotate(270);
				break;
			case R.id.rotate_right:
				rotate(90);
				break;
			default:
				break;
		}
	}
	
	public void rotate(float degree){
		Matrix matrix = new Matrix();
		matrix.postRotate(degree);
		Bitmap mBitmap2 = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		if (mBitmap != mBitmap2) {
			mBitmap.recycle();
			mBitmap = mBitmap2;
		}
		mBitmap = mBitmap2;
		mImageView.cleanHighlights();
		initHighlighting();
	}

	// And to convert the image URI to the direct file system path of the image file
	public String getRealPathFromURI(Uri contentUri) {
		// can post image
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(contentUri, proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private void initHighlighting() {
		// TODO rewrite this method to use correct aspect ratio
		mImageView.setImageBitmapResetBase(mBitmap, true);
		HighlightView hv = new HighlightView(mImageView);

		int width = mBitmap.getWidth();
		int height = mBitmap.getHeight();

		Rect imageRect = new Rect(0, 0, width, height);

		// make the default size about 4/5 of the width or height
		int cropWidth = Math.min(width, height) * 4 / 5;
		int cropHeight = cropWidth;

		if (mAspectX != 0 && mAspectY != 0) {
			if (mAspectX > mAspectY) {
				cropHeight = cropWidth * mAspectY / mAspectX;
			} else {
				cropWidth = cropHeight * mAspectX / mAspectY;
			}
		}

		int x = (width - cropWidth) / 2;
		int y = (height - cropHeight) / 2;

		RectF cropRect = new RectF(x, y, x + cropWidth, y + cropHeight);
		hv.setup(mImageView.getImageMatrix(), imageRect, cropRect, mCircleCrop, mAspectX != 0 && mAspectY != 0);
		mImageView.add(hv);

		mImageView.invalidate();
		if (mImageView.mHighlightViews.size() == 1) {
			mCrop = mImageView.mHighlightViews.get(0);
			mCrop.setFocus(true);
		}
	}

	private void onSaveClicked() {
		// TODO this code needs to change to use the decode/crop/encode single
		// step api so that we don't require that the whole (possibly large)
		// bitmap doesn't have to be read into memory
		if (mCrop == null) {
			return;
		}

		if (mSaving) {
			return;
		}
		mSaving = true;

		Bitmap croppedImage;

		// If the output is required to a specific size, create an new image
		// with the cropped image in the center and the extra space filled.
		if (mOutputX != 0 && mOutputY != 0 && !mScale) {
			// Don't scale the image but instead fill it so it's the
			// required dimension
			croppedImage = Bitmap.createBitmap(mOutputX, mOutputY, Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(croppedImage);

			Rect srcRect = mCrop.getCropRect();
			Rect dstRect = new Rect(0, 0, mOutputX, mOutputY);

			int dx = (srcRect.width() - dstRect.width()) / 2;
			int dy = (srcRect.height() - dstRect.height()) / 2;

			// If the srcRect is too big, use the center part of it.
			srcRect.inset(Math.max(0, dx), Math.max(0, dy));

			// If the dstRect is too big, use the center part of it.
			dstRect.inset(Math.max(0, -dx), Math.max(0, -dy));

			// Draw the cropped bitmap in the center
			canvas.drawBitmap(mBitmap, srcRect, dstRect, null);

			// Release bitmap memory as soon as possible
			mImageView.clear();
			mBitmap.recycle();
		} else {
			Rect r = mCrop.getCropRect();

			int width = r.width();
			int height = r.height();

			// If we are circle cropping, we want alpha channel, which is the
			// third param here.
			croppedImage = Bitmap.createBitmap(width, height, mCircleCrop ? Bitmap.Config.ARGB_8888
					: Bitmap.Config.RGB_565);

			Canvas canvas = new Canvas(croppedImage);
			Rect dstRect = new Rect(0, 0, width, height);
			canvas.drawBitmap(mBitmap, r, dstRect, null);

			// Release bitmap memory as soon as possible
			mImageView.clear();
			mBitmap.recycle();

			if (mCircleCrop) {
				// OK, so what's all this about?
				// Bitmaps are inherently rectangular but we want to return
				// something that's basically a circle. So we fill in the
				// area around the circle with alpha. Note the all important
				// PortDuff.Mode.CLEAR.
				Canvas c = new Canvas(croppedImage);
				Path p = new Path();
				p.addCircle(width / 2F, height / 2F, width / 2F, Path.Direction.CW);
				c.clipPath(p, Region.Op.DIFFERENCE);
				c.drawColor(0x00000000, PorterDuff.Mode.CLEAR);
			}

			// If the required dimension is specified, scale the image.
			if (mOutputX != 0 && mOutputY != 0 && mScale) {
				croppedImage = Util.transform(new Matrix(), croppedImage, mOutputX, mOutputY, mScaleUp,
						Util.RECYCLE_INPUT);
			}
		}

		mImageView.setImageBitmapResetBase(croppedImage, true);
		mImageView.center(true, true);
		mImageView.mHighlightViews.clear();

		Bundle extras = new Bundle();
		extras.putParcelable("data", croppedImage);
		setResult(RESULT_OK, (new Intent()).setAction("inline-data").putExtras(extras));
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
