/**
 * Created at 09.12.2010
 */
package com.yabby.storybook.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Scroller;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class BookSurfaceView extends SurfaceView implements SurfaceHolder.Callback, OnTouchListener {

	public interface BitmapProvider {
		int getBitmapsCount();

		Bitmap getBitmap(int index);
	}

	private SurfaceHolder mHolder;
	private BookPageTurnEffect mBookPageTurnEffect;
	private BitmapProvider bitmapProvider;
	private int currentIndex = 0;

	private Bitmap currentBitmap;
	private int width;
	private int height;

	private boolean flipping = false;
	private boolean forward = false;
	private long lastActionTime = 0;

	public Bitmap getCurrentBitmap() {
		if (currentBitmap == null) {
			currentBitmap = bitmapProvider.getBitmap(currentIndex);
		}
		return currentBitmap;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
		currentBitmap = bitmapProvider.getBitmap(currentIndex);
		if (this.mHolder != null) {
			Canvas c = this.mHolder.lockCanvas();
			if (c != null) {
				c.drawBitmap(currentBitmap, 0, 0, null);
				c.save();
				this.mHolder.unlockCanvasAndPost(c);
			}
		}
	}

	public void setBitmapProvider(BitmapProvider bitmapProvider) {
		this.bitmapProvider = bitmapProvider;
	}

	public BookSurfaceView(Context context) {
		super(context);
	}

	public BookSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void init(int width, int height) {
		this.width = width;
		this.height = height;
		mHolder = getHolder();
		mHolder.addCallback(this);
		mBookPageTurnEffect = new BookPageTurnEffect(new Scroller(getContext()), width, height);
		setOnTouchListener(this);
		setCurrentIndex(currentIndex);
	}

	@Override
	public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {
		Canvas c = this.mHolder.lockCanvas();
		if (c != null) {
			c.drawBitmap(currentBitmap, 0, 0, null);
			c.save();
			this.mHolder.unlockCanvasAndPost(c);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {

	}

	@Override
	public boolean onTouch(View paramView, MotionEvent me) {
		Canvas c;
		switch (me.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (me.getX() > width * 3 / 4) {
					if (bitmapProvider.getBitmapsCount() > currentIndex + 1) {
						c = this.mHolder.lockCanvas();
						mBookPageTurnEffect.startFlip(me.getX(), me.getY(), width, height, currentBitmap, bitmapProvider
								.getBitmap(currentIndex + 1), true);
						flipping = true;
						mBookPageTurnEffect.draw(c);
						forward = true;
						this.mHolder.unlockCanvasAndPost(c);
						return true;
					}
				} else if (me.getX() < width / 4) {
					if (currentIndex > 0) {
						c = this.mHolder.lockCanvas();
						mBookPageTurnEffect.startFlip(me.getX(), me.getY(), width, height, currentBitmap, bitmapProvider
								.getBitmap(currentIndex - 1), false);
						flipping = true;
						mBookPageTurnEffect.draw(c);
						forward = false;
						this.mHolder.unlockCanvasAndPost(c);
						return true;
					}
				}
				break;
			case MotionEvent.ACTION_MOVE:
				if (flipping) {
					long ct = System.currentTimeMillis();
					// ~60fps
					if (ct - lastActionTime > 17) {
						c = this.mHolder.lockCanvas();
						c.drawBitmap(getCurrentBitmap(), 0, 0, null);
						lastActionTime = ct;
						mBookPageTurnEffect.fliping(me.getX(), me.getY());
						mBookPageTurnEffect.draw(c);
						this.mHolder.unlockCanvasAndPost(c);
					}
					return true;
				}
				break;
			case MotionEvent.ACTION_OUTSIDE:
			case MotionEvent.ACTION_UP:
				if (flipping) {
					c = this.mHolder.lockCanvas();
					mBookPageTurnEffect.endFlip();
					flipping = false;
					if((forward && me.getX() < width / 4)||(!forward && me.getX() > width * 3 / 4)){
						if (forward) {
							currentIndex++;
						} else {
							currentIndex--;
						}
						currentBitmap = mBookPageTurnEffect.mBackBitmap;
					}
					c.drawBitmap(getCurrentBitmap(), 0, 0, null);
					c.save();
					this.mHolder.unlockCanvasAndPost(c);
					return true;
				}
				break;
		}
		return false;
	}

	private int x, y;

	public void nextSlide() {
		if (bitmapProvider.getBitmapsCount() > currentIndex + 1) {
			Canvas c = this.mHolder.lockCanvas();
			x = width;
			y = height - 2;
			mBookPageTurnEffect.startFlip(x, y, width, height, currentBitmap, bitmapProvider.getBitmap(currentIndex + 1), true);
			mBookPageTurnEffect.draw(c);
			this.mHolder.unlockCanvasAndPost(c);
			final Timer t = new Timer();
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					Canvas c = mHolder.lockCanvas();
					x -= 20;
					y -= 2;
					if (x <= 0) {
						mBookPageTurnEffect.endFlip();
						flipping = false;
						if (forward) {
							currentIndex++;
						} else {
							currentIndex--;
						}
						currentBitmap = mBookPageTurnEffect.mBackBitmap;
						c.drawBitmap(getCurrentBitmap(), 0, 0, null);
						t.cancel();
					} else {
						c.drawBitmap(getCurrentBitmap(), 0, 0, null);
						mBookPageTurnEffect.fliping(x, y);
						mBookPageTurnEffect.draw(c);
					}
					mHolder.unlockCanvasAndPost(c);
				}
			}, 10, 17);
		}
	}
}
