/**
 * Created at 08.12.2010
 */
package com.yabby.storybook.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.util.Log;
import android.widget.Scroller;

public class BookPageTurnEffect {

	static final int BACK_SHADOW_COLOR_END = 0x111111;
	static final int BACK_SHADOW_COLOR_END_1 = 0x888888;
	static final int BACK_SHADOW_COLOR_START = 0xff111111;
	static final int BACK_SHADOW_COLOR_START_1 = 0x888888;
	static final int FOLDER_SHADOW_COLOR_END = 0xb0333333;
	static final int FOLDER_SHADOW_COLOR_START = 0x333333;
	static final int FRONT_SHADOW_COLOR_END = 0x888888;
	static final int FRONT_SHADOW_COLOR_END_1 = 0x222222;
	static final int FRONT_SHADOW_COLOR_START = 0x80888888;
	static final int FRONT_SHADOW_COLOR_START_1 = 0x80222222;
	static final int FRONT_SHADOW_WIDTH_EDPI = 35;
	static final int FRONT_SHADOW_WIDTH_HDPI = 25;
	static final int FRONT_SHADOW_WIDTH_MDPI = 15;
	static final double RADIUS_90 = 0.78539816339744828D;
	Bitmap mBackBitmap;
	Bitmap mFrontBitmap;
	int mBackShadowColors[];
	GradientDrawable mBackShadowDrawableLR;
	GradientDrawable mBackShadowDrawableRL;
	ColorMatrixColorFilter mColorMatrixFilter;
	int mCornerX;
	int mCornerY;
	int mFolderBgColor;
	int mFolderShadowColors[];
	GradientDrawable mFolderShadowDrawableLR;
	GradientDrawable mFolderShadowDrawableRL;
	float mFolderX1;
	float mFolderX11;
	float mFolderX13;
	float mFolderX2;
	float mFolderX22;
	float mFolderX24;
	float mFolderX33;
	float mFolderX44;
	float mFolderY1;
	float mFolderY11;
	float mFolderY13;
	float mFolderY2;
	float mFolderY22;
	float mFolderY24;
	float mFolderY33;
	float mFolderY44;
	int mFrontShadowColors[];
	GradientDrawable mFrontShadowDrawableHBT;
	GradientDrawable mFrontShadowDrawableHTB;
	GradientDrawable mFrontShadowDrawableVLR;
	GradientDrawable mFrontShadowDrawableVRL;
	int mHeight;
	boolean mIsLBorRT;
	Matrix mMatrix;
	float mMatrixArray[];
	int mMaxFrontShadowW;
	float mMaxLength;
	float mMiddleX;
	float mMiddleY;
	Paint mPaint;
	Path mPath0;
	Path mPath1;
	Scroller mScroller;
	float mShadowX1;
	float mShadowX2;
	float mShadowY1;
	float mShadowY2;
	boolean mStarted;
	float mTouchToCornerDis;
	float mTouchX;
	float mTouchY;
	int mWidth;

	public BookPageTurnEffect(int w, int h) {
		mPath0 = new Path();
		mPath1 = new Path();
		mMatrix = new Matrix();
		mMatrixArray = new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0x3f800000 };
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setStyle(android.graphics.Paint.Style.FILL);
		mPaint.setStrokeWidth(0);
		createDrawable();
		ColorMatrix mColorMatrixFilter = new ColorMatrix();
		float af1[] = { 0x3f0ccccd, 0, 0, 0, 0x42a00000, 0, 0x3f0ccccd, 0, 0, 0x42a00000, 0, 0, 0x3f0ccccd, 0, 0x42a00000, 0, 0, 0,
				0x3e4ccccd, 0 };
		mColorMatrixFilter.set(af1);
		mMaxFrontShadowW = 15;
		int k = Math.max(w, h);
		if (k < 800 || k >= 1024) {
			if (k > 1024) {
				mMaxFrontShadowW = 35;
			}
			return;
		}
		mMaxFrontShadowW = 25;
		return;
	}

	public BookPageTurnEffect(Scroller scroller, int w, int h) {
		this(w, h);
		mScroller = scroller;
	}

	private final void calcCornerXY(float f, float f1) {
		if (f < (mWidth / 2)) {
			mCornerX = 0;
		} else {
			mCornerX = mWidth;
		}
		if (f1 < (mHeight / 2)) {
			mCornerY = 0;
		} else {
			mCornerY = mHeight;
		}
		if (mCornerX == 0) {
			if (mCornerY == mHeight) {
				mIsLBorRT = true;
				return;
			}
		}
		if (mCornerY == 0) {
			if (mCornerX == mWidth) {
				mIsLBorRT = true;
				return;
			}
		}
		mIsLBorRT = false;
		return;
	}

	private void calcInsectionPoint() {
		float f2 = mFolderX11 - mFolderX22;
		float f5 = mFolderY11 - mFolderY22;
		float f8 = mFolderX1 - mTouchX;
		float f11 = mFolderY1 - mTouchY;
		float f14 = (f2 * f11) - (f5 * f8);
		float f21 = (mFolderX11 * mFolderY22) - (mFolderY11 * mFolderX22);
		float f28 = (mFolderX1 * mTouchY) - (mFolderY1 * mTouchX);
		mFolderX33 = ((f21 * f8) - (f28 * f2)) / f14;
		mFolderY33 = ((f21 * f11) - (f28 * f5)) / f14;
		float f47 = (mFolderX2 * mTouchY) - (mFolderY2 * mTouchX);
		mFolderX44 = ((f21 * (mFolderX2 - mTouchX)) - (f47 * f2)) / f14;
		mFolderY44 = ((f21 * (mFolderY2 - mTouchY)) - (f47 * f5)) / f14;
	}

	private void calcPoints() {
		if (this.mTouchY < 10.0F) {
			this.mTouchY = 0.0F;
		}
		if (this.mTouchY > this.mHeight - 10) {
			this.mTouchY = this.mHeight;
		}
		this.mMiddleX = (mTouchX + mCornerX) / 2.0F;
		this.mMiddleY = (mTouchY + mCornerY) / 2.0F;
		float f12 = mMiddleX - mCornerX;
		float f15 = mMiddleY - mCornerY;
		this.mFolderY2 = mCornerY;
		this.mFolderX2 = mMiddleX + f15 * f15 / f12;
		this.mFolderY22 = mCornerY;
		this.mFolderX22 = mFolderX2 + (mFolderX2 - mCornerX) / 2.0F;
		boolean bool = isAnimation();
		if (!bool) {
			if (this.mFolderX22 < 0.0F || mFolderX22 > mWidth) {
				if (this.mFolderX22 < 0.0F) {
					this.mFolderX22 = mWidth - mFolderX22;
				}
				float f33 = Math.abs(mCornerX - mTouchX);
				this.mTouchX = Math.abs(mCornerX - this.mWidth * f33 / mFolderX22);
				this.mTouchY = Math.abs(mCornerY - Math.abs(mCornerY - mTouchY) * Math.abs(mCornerX - mTouchX) / f33);
				this.mMiddleX = (mTouchX + mCornerX) / 2.0F;
				this.mMiddleY = (mTouchY + mCornerY) / 2.0F;
				f12 = mMiddleX - mCornerX;
				f15 = mMiddleY - mCornerY;
				this.mFolderX2 = mMiddleX + f15 * f15 / f12;
				this.mFolderX22 = mFolderX2 + (mFolderX2 - mCornerX) / 2.0F;
			}
		}
		this.mFolderX1 = this.mCornerX;
		if (this.mTouchY > 0.0F) {
			if (this.mTouchY < this.mHeight) {
				this.mFolderY1 = this.mMiddleY + (f12 * f12 / f15);
				this.mFolderX11 = this.mCornerX;
				if (this.mTouchY <= 0.0F || this.mTouchY >= this.mHeight) {
					this.mFolderY11 = this.mHeight - this.mTouchY;
				} else {
					this.mFolderY11 = this.mFolderY1 + ((this.mFolderY1 - this.mCornerY) / 2.0F);
				}
			}
		}
		label1123: {
			this.mTouchToCornerDis = (float) Math.hypot((this.mTouchX - this.mCornerX), (this.mTouchY - this.mCornerY));
			this.mShadowX1 = this.mCornerX;
			this.mShadowY1 = (this.mFolderY1 + this.mFolderY11) / 2.0F;
			this.mShadowY2 = this.mCornerY;
			this.mShadowX2 = (this.mFolderX2 + this.mFolderX22) / 2.0F;
			if (this.mTouchY > 0.0F) {
				if (this.mTouchY < this.mHeight) {
					calcInsectionPoint();
				}
			}
			if (bool) {
				if (this.mCornerX <= 0) {
					if (this.mFolderX2 >= this.mWidth) {
						;
					}
				} else {
					if ((this.mCornerX <= 0) || (this.mFolderX2 > 0.0F)) {
						break label1123;
					}
				}
				this.mStarted = false;
				this.mScroller.abortAnimation();
			}
		}
	}

	private void createDrawable() {
		int ai[] = { 0x333333, 0xb0333333 };
		mFolderShadowDrawableRL = new GradientDrawable(Orientation.RIGHT_LEFT, ai);
		mFolderShadowDrawableRL.setGradientType(0);
		mFolderShadowDrawableLR = new GradientDrawable(Orientation.LEFT_RIGHT, ai);
		mFolderShadowDrawableLR.setGradientType(0);
		mBackShadowColors = new int[] { 0xff111111, 0x111111 };
		mBackShadowDrawableRL = new GradientDrawable(Orientation.RIGHT_LEFT, mBackShadowColors);
		mBackShadowDrawableRL.setGradientType(0);
		mBackShadowDrawableLR = new GradientDrawable(Orientation.LEFT_RIGHT, mBackShadowColors);
		mBackShadowDrawableLR.setGradientType(0);
		mFrontShadowColors = new int[] { 0x80888888, 0x888888 };
		mFrontShadowDrawableVLR = new GradientDrawable(Orientation.LEFT_RIGHT, mFrontShadowColors);
		mFrontShadowDrawableVLR.setGradientType(0);
		mFrontShadowDrawableVRL = new GradientDrawable(Orientation.RIGHT_LEFT, mFrontShadowColors);
		mFrontShadowDrawableVRL.setGradientType(0);
		mFrontShadowDrawableHTB = new GradientDrawable(Orientation.TOP_BOTTOM, mFrontShadowColors);
		mFrontShadowDrawableHTB.setGradientType(0);
		mFrontShadowDrawableHBT = new GradientDrawable(Orientation.BOTTOM_TOP, mFrontShadowColors);
		mFrontShadowDrawableHBT.setGradientType(0);
	}

	private void drawByDegree(Canvas canvas) {
		mPath0.reset();
		mPath0.moveTo(mFolderX22, mFolderY22);
		mPath0.quadTo(mFolderX2, mFolderY2, mFolderX44, mFolderY44);
		mPath0.lineTo(mTouchX, mTouchY);
		mPath0.lineTo(mFolderX33, mFolderY33);
		mPath0.quadTo(mFolderX1, mFolderY1, mFolderX11, mFolderY11);
		mPath0.lineTo(mCornerX, mCornerY);
		mPath0.close();
		canvas.save();
		canvas.clipPath(mPath0, android.graphics.Region.Op.XOR);
		canvas.drawBitmap(mFrontBitmap, 0F, 0F, mPaint);
		canvas.restore();
		float f16 = drawBackPageAndShadow(canvas);
		drawFrontShadow(canvas, f16);
		drawFolderShadow(canvas, f16);
	}

	private void drawVertical(Canvas canvas) {
		mPath0.reset();
		mPath0.moveTo(mCornerX, 0);
		mPath0.lineTo(mCornerX, mHeight);
		mPath0.lineTo(mFolderX22, mHeight);
		mPath0.lineTo(mFolderX22, 0);
		mPath0.close();
		canvas.save();
		canvas.clipPath(mPath0);

		canvas.drawBitmap(mBackBitmap, 0, 0, mPaint);
		canvas.restore();

		int i = (int) (mMiddleX - 0.5D);
		int j = (int) (mFolderX22 + 0.5D);
		GradientDrawable gradientdrawable = mBackShadowDrawableRL;
		if (mCornerX > 0) {
			i = (int) (mFolderX22 - 0.5D);
			j = (int) (mMiddleX + 0.5D);
			gradientdrawable = mBackShadowDrawableLR;
		}
		gradientdrawable.setBounds(i, 0, j, mHeight);
		gradientdrawable.draw(canvas);
		mPath0.reset();
		mPath0.moveTo(mFolderX22, 0);
		mPath0.lineTo(mWidth - mCornerX, 0);
		mPath0.lineTo(mWidth - mCornerX, mHeight);
		mPath0.lineTo(mFolderX22, mHeight);
		canvas.save();

		canvas.clipPath(mPath0);
		canvas.drawBitmap(mFrontBitmap, 0, 0, mPaint);
		canvas.restore();
		float f14 = mTouchToCornerDis / 10;
		i = (int) (mTouchX - 0.5D);
		j = (int) ((i + f14) + 0.5D);
		gradientdrawable = mFrontShadowDrawableVLR;
		if (mCornerX > 0) {
			i = (int) ((mTouchX - f14) - 0.5D);
			j = (int) (mTouchX + 0.5D);
			gradientdrawable = mFrontShadowDrawableVRL;
		}
		gradientdrawable.setBounds(i, 0, j, mHeight);
		gradientdrawable.draw(canvas);
		float f17 = (mFolderX22 + mMiddleX) / 2;
		mPath0.reset();
		mPath0.moveTo(f17, 0);
		mPath0.lineTo(mTouchX, 0);
		mPath0.lineTo(mTouchX, mHeight);
		mPath0.lineTo(f17, mHeight);
		mPath0.lineTo(f17, 0);
		canvas.save();
		canvas.clipPath(mPath0);

		mPaint.setStrokeWidth(0);
		mPaint.setColorFilter(null);
		mPaint.setAntiAlias(true);
		mPaint.setStyle(android.graphics.Paint.Style.FILL);

//		Matrix matrix = new Matrix();
//		matrix.preScale(-1, 1);
//		matrix.postTranslate(mCornerX + mTouchX, 0);
//		canvas.drawBitmap(mFrontBitmap, matrix, mPaint);
		mPaint.setColor(Color.GRAY);
		canvas.drawPaint(mPaint);
		canvas.restore();

		i = (int) f17;
		j = (int) (((mMiddleX + mTouchX) / 2F) + 0.5D);
		gradientdrawable = mFolderShadowDrawableRL;
		if (mCornerX > 0) {
			i = (int) ((mMiddleX + mTouchX) / 2F);
			j = (int) (f17 + 0.5D);
			gradientdrawable = mFolderShadowDrawableLR;
		}
		gradientdrawable.setBounds(i, 0, j, mHeight);
		gradientdrawable.draw(canvas);
	}

	public final void abortAnimation() {
		mScroller.abortAnimation();
		mStarted = false;
	}

	public final boolean canDragOver() {
		if (mCornerX > 0) {
			if (mTouchX < mWidth - mWidth / 10) {
				return true;
			}
		}
		if (mCornerX > 0) {
			return false;
		}
		if (mTouchX > mWidth / 10) {
			return true;
		} else {
			return false;
		}
	}

	public final boolean doAnimation() {
		if (mScroller != null) {
			mScroller.computeScrollOffset();
			float f = mScroller.getCurrX();
			mTouchX = f;
			float f1 = mScroller.getCurrY();
			mTouchY = f1;
			calcPoints();
		}
		boolean flag;
		if (mScroller.isFinished()) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}

	public final void draw(Canvas canvas) {
		if (!(mFrontBitmap != null && mBackBitmap != null)) {
			return;
		}
		if (mTouchY > 0 && mTouchY < mHeight) {
			drawByDegree(canvas);
		} else {
			Log.i("BookPageTurnEffect", "drawVertical");
			drawVertical(canvas);
		}
	}

	public float drawBackPageAndShadow(Canvas canvas) {
		float res = (float) Math.toDegrees(Math.atan2(mFolderX2 - mCornerX, mFolderY1 - mCornerY));
		float f16 = mFolderX2 + (mFolderX2 - mCornerX) / 2;
		float f17 = mTouchToCornerDis / 4 + 1;
		mFolderX13 = (2F * mFolderX1 + mFolderX33 + mFolderX11) / 4F;
		mFolderY13 = (2F * mFolderY1 + mFolderY11 + mFolderY33) / 4F;
		mFolderX24 = (2F * mFolderX2 + mFolderX22 + mFolderX44) / 4F;
		mFolderY24 = (2F * mFolderY2 + mFolderY22 + mFolderY44) / 4F;
		int i = (int) (mFolderY2 - 1F);
		int j = (int) (mMaxLength + mFolderY2);

		int k;
		int l;
		GradientDrawable gradientdrawable;
		if (mIsLBorRT) {
			k = (int) (f16 - 1);
			l = (int) (f16 + f17 + 1);
			gradientdrawable = mBackShadowDrawableLR;
		} else {
			k = (int) (f16 - f17 - 1);
			l = (int) (1 + f16);
			gradientdrawable = mBackShadowDrawableRL;
		}
		mPath1.reset();
		mPath1.moveTo(mFolderX11, mFolderY11);
		mPath1.lineTo(mFolderX13, mFolderY13);
		mPath1.lineTo(mFolderX24, mFolderY24);
		mPath1.lineTo(mFolderX22, mFolderY22);
		mPath1.lineTo(mCornerX, mCornerY);
		mPath1.close();
		canvas.save();

		canvas.clipPath(mPath0);
		canvas.clipPath(mPath1, android.graphics.Region.Op.INTERSECT);
		canvas.drawBitmap(mBackBitmap, 0, 0, mPaint);
		canvas.rotate(res, f16, mFolderY2);
		gradientdrawable.setBounds(k, i, l, j);
		gradientdrawable.draw(canvas);
		canvas.restore();
		return res;
	}

	public void drawFolderShadow(Canvas canvas, float f) {
		float f7 = Math.min(Math.abs(mShadowX2 - mFolderX2), Math.abs(mShadowY1 - mFolderY1));
		mPath1.reset();
		mPath1.moveTo(mFolderX13, mFolderY13);
		mPath1.lineTo(mFolderX24, mFolderY24);
		mPath1.lineTo(mFolderX44, mFolderY44);
		mPath1.lineTo(mTouchX, mTouchY);
		mPath1.lineTo(mFolderX33, mFolderY33);
		mPath1.close();

		int i = (int) (mFolderY22 - 1);
		int j = (int) (mFolderY22 + mMaxLength);
		GradientDrawable gradientdrawable;
		int k;
		int l;
		if (mIsLBorRT) {
			k = (int) (mFolderX22 - 1F);
			l = (int) (mFolderX22 + f7 + 1F);
			gradientdrawable = mFolderShadowDrawableLR;
		} else {
			k = (int) (mFolderX22 - f7 - 1F);
			l = (int) (mFolderX22 + 1F);
			gradientdrawable = mFolderShadowDrawableRL;
		}
		canvas.save();
		
		canvas.clipPath(mPath0);
		canvas.clipPath(mPath1, android.graphics.Region.Op.INTERSECT);
//		mPaint.setColor(mFolderBgColor);
		canvas.drawPaint(mPaint);

//		mPaint.setColorFilter(mColorMatrixFilter);
		
//		float f22 = mCornerX - mFolderX2;
//		float f25 = mFolderY1 - mCornerY;
//		float f26 = (float) Math.hypot(f22, f25);
//		float f27 = f22 / f26;
//		float f28 = f25 / f26;
//		float f31 = 2F * f28 * f27;
//		mMatrixArray[0] = 1 - 2 * f28 * f28;
//		mMatrixArray[1] = f31;
//		mMatrixArray[3] = f31;
//		mMatrixArray[4] = 1F - 2F * f27 * f27;
//		mMatrix.reset();
//		mMatrix.setValues(new float[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0x3f800000 });
//		mMatrix.preTranslate(-mFolderX2, -mFolderY2);
//		mMatrix.postTranslate(mFolderX2, mFolderY2);
		mPaint.setColor(Color.GRAY);
		canvas.drawPaint(mPaint);
//		canvas.drawBitmap(mFrontBitmap, mMatrix, mPaint);
		
		mPaint.setColorFilter(null);
		canvas.rotate(f, mFolderX22, mFolderY22);
		gradientdrawable.setBounds(k, i, l, j);
		gradientdrawable.draw(canvas);
		canvas.restore();
	}

	public void drawFrontShadow(Canvas canvas, float f) {
		float f9 = Math.min(mTouchToCornerDis / 10, mMaxFrontShadowW);
		float f10 = f9 * 1.414F;
		GradientDrawable gradientdrawable;
		float f13;
		if (mIsLBorRT) {
			float f11 = mFolderY1;
			float f12 = mTouchY;
			f13 = f11 - f12;
		} else {
			float f61 = mTouchY;
			float f62 = mFolderY1;
			f13 = f61 - f62;
		}
		double d2 = Math.atan2(f13, mTouchX - mFolderX1) + 0.78539816339744828D;
		float f16 = (float) (mTouchX + Math.cos(d2) * f10);
		float f17 = (float) Math.sin(d2) * f10;
		float f18;
		if (mIsLBorRT) {
			f18 = mTouchY - f17;
		} else {
			f18 = mTouchY + f17;
		}
		float f21 = (float) Math.hypot(f16 - mFolderX1, f18 - mFolderY1) + 1;
		float f24 = (float) Math.hypot(f16 - mFolderX2, f18 - mFolderY2) + 1;
		int i;
		int j;
		int k;
		int l;
		if (mIsLBorRT) {
			i = (int) (mFolderX2 - f9 - 1);
			j = (int) (mFolderX2 + 1);
			k = (int) (mFolderY2 - 1);
			l = (int) (mFolderY2 + f24 + 1);
			gradientdrawable = mFrontShadowDrawableVRL;
		} else {
			i = (int) (mFolderX2 - 1);
			j = (int) (mFolderX2 + f9 + 1);
			k = (int) (mFolderY2 - 1);
			l = (int) (mFolderY2 + f24 + 1);
			gradientdrawable = mFrontShadowDrawableVLR;
		}
		mPath1.reset();
		mPath1.moveTo(f16, f18);
		mPath1.lineTo(mTouchX, mTouchY);
		mPath1.lineTo(mFolderX2, mFolderY2);
		mPath1.lineTo(mFolderX22, mFolderY22);
		mPath1.close();
		canvas.save();
		canvas.clipPath(mPath0, android.graphics.Region.Op.XOR);
		canvas.clipPath(mPath1, android.graphics.Region.Op.INTERSECT);
		canvas.rotate((float) Math.toDegrees(Math.atan2(mFolderX2 - mTouchX, mTouchY - mFolderY2)), mFolderX2, mFolderY2);
		gradientdrawable.setBounds(i, k, j, l);
		gradientdrawable.draw(canvas);
		canvas.restore();
		mPath1.reset();
		mPath1.moveTo(f16, f18);
		mPath1.lineTo(mTouchX, mTouchY);
		mPath1.lineTo(mFolderX1, mFolderY1);
		mPath1.lineTo(mFolderX11, mFolderY11);
		mPath1.close();
		if (mIsLBorRT) {
			k = (int) (mFolderY1 - 1);
			i = (int) (mFolderX1 - f21 - 1);
			j = (int) (mFolderX1 + 1);
			l = (int) (mFolderY1 + f9 + 1);
			gradientdrawable = mFrontShadowDrawableHTB;
		} else {
			i = (int) (mFolderX1 - f21 - 1);
			k = (int) (mFolderY1 - f9 - 1);
			j = (int) (mFolderX1 + 1);
			l = (int) (mFolderY1 + 1);
			gradientdrawable = mFrontShadowDrawableHBT;
		}
		canvas.save();
		canvas.clipPath(mPath0, android.graphics.Region.Op.XOR);
		canvas.clipPath(mPath1, android.graphics.Region.Op.INTERSECT);
		canvas.rotate((float) Math.toDegrees(Math.atan2(mFolderY1 - mTouchY, mFolderX1 - mTouchX)), mFolderX1, mFolderY1);
		gradientdrawable.setBounds(i, k, j, l);
		gradientdrawable.draw(canvas);
		canvas.restore();
	}

	public final boolean endFlip() {
		boolean flag;
		if (mStarted) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}

	public final void fliping(float f, float f1) {
		mTouchX = f;
		mTouchY = f1;
		calcPoints();
	}

	public final Bitmap getBitmap() {
		return mFrontBitmap;
	}

	public final boolean isAnimation() {
		boolean flag;
		if (mScroller != null && !mScroller.isFinished()) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	public void setFolderBgColor(int i) {
		if (i == 0x111111) {
			mBackShadowColors[0] = 0xff888888;
			mBackShadowColors[1] = 0x888888;
		} else if (mBackShadowColors[0] != 0xff111111) {
			mBackShadowColors[0] = 0xff111111;
			mBackShadowColors[1] = 0x111111;
		}
		if (i <= 0x888888) {
			mFrontShadowColors[0] = 0x80222222;
			mFrontShadowColors[1] = 0x222222;
		} else if (mFrontShadowColors[0] != 0x80888888) {
			mFrontShadowColors[0] = 0x80888888;
			mFrontShadowColors[1] = 0x888888;
		}
		int j = (int) (Color.red(i) * 0.55F + 80F) & 0xff;
		int k = (int) (Color.green(i) * 0.55F + 80F) & 0xff;
		int l = (int) (Color.blue(i) * 0.55F + 80F) & 0xff;
		int i1 = j << 16;
		int j1 = 0xff000000 | i1;
		int k1 = k << 8;
		int l1 = j1 | k1 | l;
		mFolderBgColor = l1;
	}

	public void startAnimation(Context context, int i) {
		if (mScroller == null) {
			mScroller = new Scroller(context);
		}
		int l;
		int k1;
		if (mCornerX > 0) {
			l = -(int) (mWidth + mTouchX);
		} else {
			l = (mWidth * 2) - (int) mTouchX;
		}
		if (mCornerY > 0) {
			k1 = mCornerY - (int) mTouchY;
		} else {
			k1 = -(int) mTouchY;
		}
		mScroller.startScroll((int) mTouchX, (int) mTouchY, l, k1, i);
	}

	public final void startFlip(float f, float f1, int i, int j, Bitmap bitmap, Bitmap bitmap1) {
		if (f < 5F) {
			f = 5F;
		}
		if (f >= i - 5F) {
			f = i - 5F;
		}
		mWidth = i;
		mHeight = j;
		mTouchX = f;
		mTouchY = f1;
		mStarted = true;
		mFrontBitmap = bitmap;
		mBackBitmap = bitmap1;
		mMaxLength = (float) Math.hypot(i, j);
		calcCornerXY(f, f1);
		calcPoints();
	}

	public final void startFlip(float f, float f1, int i, int j, Bitmap bitmap, Bitmap bitmap1, boolean flag) {
		if (f < 5F) {
			f = 5F;
		}
		float f2 = i - 5F;
		if (f >= f2) {
			f = i - 5F;
		}
		if (flag) {
			mCornerX = i;
		} else {
			mCornerX = 0;
		}
		float f3 = j / 2;
		if (f1 < f3) {
			mCornerY = 0;
		} else {
			mCornerY = j;
		}
		mWidth = i;
		mHeight = j;
		mTouchX = f;
		mTouchY = f1;
		mStarted = true;
		mFrontBitmap = bitmap;
		mBackBitmap = bitmap1;
		mMaxLength = (float) Math.hypot(i, j);
		if (mCornerX == 0) {
			if (mCornerY == mHeight) {
				mIsLBorRT = true;
				calcPoints();
				return;
			}
		}
		if (mCornerY == 0) {
			if (mCornerX == mWidth) {
				mIsLBorRT = true;
				calcPoints();
				return;
			}
		}
		mIsLBorRT = false;
		calcPoints();
	}
}
