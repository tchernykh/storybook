/**
 * Created at 13.09.2010
 */
package com.yabby.storybook;

import android.app.Activity;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.yabby.storybook.adapter.EditBookAdapter;
import com.yabby.storybook.adapter.EditBookAdapter.EditBookHandler;
import com.yabby.storybook.data.BooksLoader;
import com.yabby.storybook.data.BooksLoader.SaveHandler;
import com.yabby.storybook.dialog.CaptureSoundDialog;
import com.yabby.storybook.dialog.CaptureSoundDialog.CaptureSoundDialogHandler;
import com.yabby.storybook.dialog.EditImageDialog;
import com.yabby.storybook.dialog.InsertPageDialog;
import com.yabby.storybook.dialog.SaveBookDialog;
import com.yabby.storybook.dialog.SetDelayDialog;
import com.yabby.storybook.dialog.SetDelayDialog.SetDelayDialogHandler;
import com.yabby.storybook.image.CropImage;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.Page;
import com.yabby.storybook.select.Action;
import com.yabby.storybook.select.CustomGallery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class EditBookActivity extends ListActivity implements EditBookHandler, SetDelayDialogHandler, CaptureSoundDialogHandler,
		SaveHandler, OnClickListener {

    private static final String LOG_TAG = EditBookActivity.class.getSimpleName();

	public static final String BOOK_PARAM = "book";

	public static final String FILE_PATH = "filePath";

	private static final int SET_DELAY_DIALOG = 0;
	private static final int RECORD_MEDIA_DIALOG = 1;
	private static final int SAVE_BOOK_DIALOG = 2;
	private static final int INSERT_NEW_PAGE_DIALOG = 3;
	private static final int CHANGE_IMAGE_DIALOG = 4;
	private static final int PROCESS_IMAGES_DIALOG = 5;

	private static final int SELECT_IMAGE_REQUEST_CODE = 1;
	private static final int EDIT_IMAGE_REQUEST_CODE = 2;
	private static final int ADD_IMAGE_REQUEST_CODE = 3;
	private static final int ADD_EDITED_IMAGE_REQUEST_CODE = 4;
	private static final int ADD_PAGES_REQUEST_CODE = 5;

	private int mCurrentPosition = -1;

	private ProgressThread mProgressThread;
	private ProgressDialog mProgressDialog;

	// Define the Handler that receives messages from the thread and update the progress
	final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			int total = msg.getData().getInt("total");
			if (mProgressDialog != null) {
				mProgressDialog.setProgress(total);
			}
			if (msg.getData().getBoolean("finished")) {
				EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
				adapter.notifyDataSetChanged();
				dismissDialog(PROCESS_IMAGES_DIALOG);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.yabby.storybook.R.layout.edit_book);
		Book book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		BooksLoader.loadBookPagesData(this, book);
		setTitle(getString(com.yabby.storybook.R.string.editing) + " " + book.getName());
		View footer = getLayoutInflater().inflate(com.yabby.storybook.R.layout.add_multi_pages, null, false);
		footer.setOnClickListener(this);
		getListView().addFooterView(footer, null, false);
		getListView().setCacheColorHint(0);
		setListAdapter(new EditBookAdapter(this, this, book));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Book book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		BooksLoader.cleanBookData(this, book);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case SELECT_IMAGE_REQUEST_CODE:
					Intent editImageIntent = new Intent(this, CropImage.class);
                    String single_path = data.getStringExtra("single_path");
					editImageIntent.putExtra(Intent.EXTRA_TEXT, single_path);
					startActivityForResult(editImageIntent, EDIT_IMAGE_REQUEST_CODE);
					break;
				case EDIT_IMAGE_REQUEST_CODE:
					try {
						Bitmap croppedImage = data.getParcelableExtra("data");
						EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
						Book book = adapter.getBook();
						Page page = book.getContent().get(mCurrentPosition);
						String pageImageName = book.getName() + "_" + mCurrentPosition + ".png";
						FileOutputStream out = new FileOutputStream(book.getPath() + "/" + pageImageName);
						croppedImage.compress(Bitmap.CompressFormat.PNG, 90, out);
						page.setImage(pageImageName);
						page.setImageHeight(croppedImage.getHeight());
						page.setImageWidth(croppedImage.getWidth());
						out.close();

						BooksLoader.loadPageCover(this, book, page);
						adapter.notifyDataSetChanged();
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case ADD_IMAGE_REQUEST_CODE:
					Intent addImageIntent = new Intent(this, CropImage.class);
					addImageIntent.setData(data.getData());
					startActivityForResult(addImageIntent, ADD_EDITED_IMAGE_REQUEST_CODE);
					break;
				case ADD_EDITED_IMAGE_REQUEST_CODE:
					try {
						Bitmap croppedImage = data.getParcelableExtra("data");
						EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
						Book book = adapter.getBook();
						String pageImageName = book.getName() + "_" + mCurrentPosition + ".png";
						FileOutputStream out = new FileOutputStream(book.getPath() + "/" + pageImageName);
						croppedImage.compress(Bitmap.CompressFormat.PNG, 90, out);
						Page page = new Page();
						page.setImage(pageImageName);
						page.setImageHeight(croppedImage.getHeight());
						page.setImageWidth(croppedImage.getWidth());
						out.close();
						BooksLoader.loadPageCover(this, book, page);
						book.getContent().add(mCurrentPosition, page);
						adapter.notifyDataSetChanged();
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case ADD_PAGES_REQUEST_CODE:
					EditBookAdapter adapter = (EditBookAdapter) getListAdapter();

                    String[] all_path = data.getStringArrayExtra("all_path");

                    ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();

                    for (String path : all_path) {
                        CustomGallery item = new CustomGallery();
                        item.sdcardPath = path;
                        dataT.add(item);
                    }

					Book book = adapter.getBook();
					mCurrentPosition = book.getContent().size();
					if (mCurrentPosition == 1) {
						Page page = book.getContent().get(0);
						if (page.getImage() == null && page.getMedia() == null && page.getDelaySec() == 0 && !dataT.isEmpty()) {
							book.getContent().clear();
							mCurrentPosition = 0;
							adapter.notifyDataSetChanged();
						}
					}
					showDialog(PROCESS_IMAGES_DIALOG);
					mProgressThread = new ProgressThread(handler, dataT, book);
					mProgressThread.start();
					mProgressDialog.setMax(dataT.size());
					break;
				default:
					break;
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Book book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		switch (id) {
			case SET_DELAY_DIALOG:
				return new SetDelayDialog(this, this);
			case RECORD_MEDIA_DIALOG:
				return new CaptureSoundDialog(this, this);
			case SAVE_BOOK_DIALOG:
				return new SaveBookDialog(this, book);
			case INSERT_NEW_PAGE_DIALOG:
				return new InsertPageDialog(this);
			case CHANGE_IMAGE_DIALOG:
				return new EditImageDialog(this);
			case PROCESS_IMAGES_DIALOG:
				mProgressDialog = new ProgressDialog(this);
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.setMessage("Processing images...");
				mProgressDialog.setCancelable(false);
				return mProgressDialog;
			default:
				break;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
		Book book = adapter.getBook();
		Page page = null;
		switch (id) {
			case SET_DELAY_DIALOG:
				if (mCurrentPosition >= 0) {
					page = book.getContent().get(mCurrentPosition);
				}
				SetDelayDialog d = (SetDelayDialog) dialog;
				d.setValue(page.getDelaySec());
				break;
			case RECORD_MEDIA_DIALOG:
				if (mCurrentPosition >= 0) {
					page = book.getContent().get(mCurrentPosition);
				}
				CaptureSoundDialog recordMediaDialog = (CaptureSoundDialog) dialog;
				recordMediaDialog.setBook(book);
				recordMediaDialog.setPage(page);
				recordMediaDialog.setFileName(book.getName() + "_" + mCurrentPosition + ".mp4");
				break;
			case INSERT_NEW_PAGE_DIALOG:
				InsertPageDialog insertPageDialog = (InsertPageDialog) dialog;
				insertPageDialog.setPosition(mCurrentPosition);
				break;
			case CHANGE_IMAGE_DIALOG:
				if (mCurrentPosition >= 0) {
					page = book.getContent().get(mCurrentPosition);
                    EditImageDialog editImageDialog = (EditImageDialog) dialog;
                    editImageDialog.showImageUrl(book.getPath() + "/" + page.getImage());
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void addPage(int pos) {
		mCurrentPosition = pos;
		showDialog(INSERT_NEW_PAGE_DIALOG);
	}

	public void addPageResult(int pos) {
		mCurrentPosition = pos;
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(i, ADD_IMAGE_REQUEST_CODE);
	}

	@Override
	public void selectImage(int pos) {
		mCurrentPosition = pos;
		EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
		Page page = adapter.getBook().getContent().get(mCurrentPosition);
		if (page.getImage() == null) {
			changeImage();
		} else {
			showDialog(CHANGE_IMAGE_DIALOG);
		}
	}

	public void changeImage() {
        startActivityForResult(new Intent(Action.ACTION_PICK), SELECT_IMAGE_REQUEST_CODE);
	}

	@Override
	public void play(int pos) {
		mCurrentPosition = pos;
		playBook(pos);
	}

	private void playBook(int pos) {
		Book book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		if (book.getContent().isEmpty()) {
			Toast.makeText(this, getString(com.yabby.storybook.R.string.empty_play_error), Toast.LENGTH_LONG).show();
			return;
		}
		for (Page page : book.getContent()) {
			if (page.getImage() == null) {
				Toast.makeText(this, getString(com.yabby.storybook.R.string.play_error), Toast.LENGTH_LONG).show();
				return;
			}
		}
		Intent playPageIntent = new Intent(this, PlayActivity.class);
		playPageIntent.putExtra(PlayActivity.BOOK_PARAM, book);
		playPageIntent.putExtra(PlayActivity.PAGE_PARAM, pos);
		startActivity(playPageIntent);
	}

	@Override
	public void setDelay(int pos) {
		mCurrentPosition = pos;
		showDialog(SET_DELAY_DIALOG);
	}

	@Override
	public void setDelayValues(int delay) {
		EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
		adapter.getBook().getContent().get(mCurrentPosition).setDelayMillis(delay);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void recordMedia(int position) {
		mCurrentPosition = position;
		showDialog(RECORD_MEDIA_DIALOG);
	}

	@Override
	public void setRecordedSoundValue(String name) {
		EditBookAdapter adapter = (EditBookAdapter) getListAdapter();
		Book book = adapter.getBook();
		book.getContent().get(mCurrentPosition).setMedia(name);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(com.yabby.storybook.R.menu.edit_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case com.yabby.storybook.R.id.open_menu:
				Intent selectBookIntent = new Intent(this, SelectBookActivity.class);
				startActivity(selectBookIntent);
				break;
			case com.yabby.storybook.R.id.save_menu:
				saveBook();
				break;
			case com.yabby.storybook.R.id.play_menu:
				playBook(0);
				break;
			case com.yabby.storybook.R.id.share_menu:
				exportAsVideo();
				// uploadOnYoutube();
				break;
		}
		return true;
	}

	private void saveBook() {
		final Book book;
		book = (Book) getIntent().getSerializableExtra(BOOK_PARAM);
		if (book.getContent().isEmpty()) {
			Toast.makeText(this, getString(com.yabby.storybook.R.string.empty_save_error), Toast.LENGTH_LONG).show();
			return;
		}
		for (Page page : book.getContent()) {
			if (page.getImage() == null) {
				Toast.makeText(this, getString(com.yabby.storybook.R.string.not_all_images_set), Toast.LENGTH_LONG).show();
				return;
			}
		}
		final String path = getIntent().getStringExtra(FILE_PATH);
		if (path != null) {
			BooksLoader.saveBookChanges(EditBookActivity.this, book, path);
		} else {
			showDialog(SAVE_BOOK_DIALOG);
		}
	}

	private void uploadOnYoutube(File video) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_SEND);
		intent.setType("video/quicktime");
		intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(video));
		try {
			startActivity(Intent.createChooser(intent, "Upload video via:"));
		} catch (ActivityNotFoundException ex) {
			Toast.makeText(this, "No way to share video!", Toast.LENGTH_SHORT).show();
		}
	}

	private void exportAsVideo() {
		BooksLoader.setSaveHandler(this);
		saveBook();
	}

	@Override
	public void onSaved(File f, Book book) {
        // disabled because server inactive currently
//		HttpClient httpclient = new DefaultHttpClient();
//		httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
//		HttpPost httppost = new HttpPost(getString(R.string.converterServiceUrl));
//		OutputStream out = null;
//		File video = null;
//		try {
//			MultipartEntity mpEntity = new MultipartEntity();
//			ContentBody cbFile = new FileBody(f, "application/book");
//			mpEntity.addPart("userfile", cbFile);
//			httppost.setEntity(mpEntity);
//			System.out.println("executing request " + httppost.getRequestLine());
//			HttpResponse response = httpclient.execute(httppost);
//			HttpEntity resEntity = response.getEntity();
//			video = new File(Utils.getAppDir(this), book.getName() + ".mov");
//			out = new FileOutputStream(video);
//			Utils.copyStream(resEntity.getContent(), out);
//			httpclient.getConnectionManager().shutdown();
//		} catch (Exception e) {
//			Log.e("EditBookActivity", "exportAsVideo", e);
//		} finally {
//			try {
//				if (out != null) {
//					out.close();
//				}
//			} catch (IOException e) {
//				Log.e("EditBookActivity", "exportAsVideo - out.close", e);
//			}
//		}
//		uploadOnYoutube(video);
	}

	@Override
	public void onClick(View paramView) {
        startActivityForResult(new Intent(Action.ACTION_MULTIPLE_PICK), ADD_PAGES_REQUEST_CODE);
	}

	/** Nested class that performs progress calculations (counting) */
	private class ProgressThread extends Thread {
		Handler mHandler;
		ArrayList<CustomGallery> images;
		Book book;

		ProgressThread(Handler h, ArrayList<CustomGallery> images, Book book) {
			mHandler = h;
			this.book = book;
			this.images = images;
		}

		@Override
		public void run() {
			int total = 0;
			try {
				for (CustomGallery image : images) {
                    String imageFilePath = image.sdcardPath;
					FileInputStream fis = null;
					FileOutputStream fos = null;
					try{
						fis = new FileInputStream(imageFilePath);
						String ext = ".png";
						int ldi = imageFilePath.lastIndexOf('.');
						if(ldi>0){
							ext = imageFilePath.substring(ldi);
						}
						String pageImageName = book.getName() + "_" + mCurrentPosition + ext;
						fos = new FileOutputStream(book.getPath() + "/" + pageImageName);
						Options opts = new Options();
						opts.inJustDecodeBounds = true;
						Utils.copyStream(fis, fos);

						BitmapFactory.decodeFile(imageFilePath, opts);
						Page page = new Page();
						page.setImage(pageImageName);
						page.setImageHeight(opts.outHeight);
						page.setImageWidth(opts.outWidth);
						BooksLoader.loadPageCover(EditBookActivity.this, book, page);
						book.getContent().add(mCurrentPosition, page);
					} catch (Exception e) {
						Log.e("EditBookActivity", "copy file problems", e);
					} finally{
						Utils.safeClose(fis);
						Utils.safeClose(fos);
					}
					mCurrentPosition++;
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putInt("total", total);
					msg.setData(b);
					mHandler.sendMessage(msg);
					total++;
				}
			} catch (Exception e) {
				Log.e("EditBookActivity", "images process", e);
			}
			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putInt("total", total);
			b.putBoolean("finished", true);
			msg.setData(b);
			mHandler.sendMessage(msg);
		}
	}

}
