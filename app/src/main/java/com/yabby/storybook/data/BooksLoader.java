/**
 * Created at 13.09.2010
 */
package com.yabby.storybook.data;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import com.yabby.storybook.R;
import com.yabby.storybook.Utils;
import com.yabby.storybook.model.Book;
import com.yabby.storybook.model.BookCover;
import com.yabby.storybook.model.Page;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * @author Chernykh Aleksei (tchernykh@gmail.com)
 */
public class BooksLoader {

    private static final String LOG_TAG = BooksLoader.class.getSimpleName();

	private final static int COVER_WIDTH = 60;
	private final static int COVER_HEIGHT = 45;
	
	public interface SaveHandler {
		void onSaved(File f, Book book);
	}

	private static SaveHandler saveHandler;
	
	private static List<BookCover> coversCache;

	public static void setSaveHandler(SaveHandler saveHandler) {
		BooksLoader.saveHandler = saveHandler;
	}

	public static List<BookCover> loadCovers(Context context) {
		if(coversCache == null){
			File appDir = Utils.getAppDir(context);
			ArrayList<String> fileNames = new ArrayList<String>();
			findFiles(appDir.listFiles(), fileNames);
			List<BookCover> result = new ArrayList<BookCover>();
			for (String fileName : fileNames) {
				if (fileName.endsWith(".book")) {
                    Log.i(LOG_TAG, "loadCover: " + fileName + " " + new File(fileName).exists());
					BookCover book = loadBookCover(context, fileName);
					if(book!=null){
						result.add(book);
					}
				}
			}
			coversCache = result;
		}
		return coversCache;
	}

	private static BookCover loadBookCover(Context context, String bookFilePath) {
		File bookFile = new File(bookFilePath);
		ZipInputStream in = null;
		BookCover result = null;
		try {
			Book book = loadBookData(bookFile);
			result = new BookCover();
			result.setBookName(book.getName());
			result.setBookPath(bookFilePath);

			// Open the ZIP file
			in = new ZipInputStream(new FileInputStream(bookFile));

			ZipEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				Page page = book.getContent().get(0);
				if (entry.getName().equals(page.getImage())) {
					result.setCover(getCoverBitmap(context, in, page.getImageWidth(), page.getImageHeight()));
					break;
				}
			}
		} catch (IOException e) {
			error(context, R.string.load_error, e);
		} finally {
            Utils.safeClose(in);
		}
		return result;
	}

	public static Bitmap getCoverBitmap(Context context, InputStream in, int width, int height) {
		BitmapFactory.Options opts = new BitmapFactory.Options();
		final float scale = context.getResources().getDisplayMetrics().density;
		int w = (int) (COVER_WIDTH /* dip */* scale + 0.5f);
		int h = (int) (COVER_HEIGHT /* dip */* scale + 0.5f);
		opts.outWidth = w;
		opts.outHeight = h;
		
		if(width!=0 && height!=0){
			int width_tmp=width, height_tmp=height;
	        int imgScale=1;
	        while(true){
	            if(width_tmp/2<w || height_tmp/2<h) {
					break;
				}
	            width_tmp/=2;
	            height_tmp/=2;
	            imgScale++;
	        }
			opts.inSampleSize=imgScale;
		} else {
			opts.inSampleSize=8;
		}
		
		opts.inTempStorage = new byte[16*1024];
		Bitmap cover = BitmapFactory.decodeStream(in, null, opts);
		return cover;
	}

	public static Book loadBook(Context context, BookCover baseData) {
		File bookFile = new File(baseData.getBookPath());
		ZipInputStream in = null;
		Book result = null;
		try {
			result = loadBookData(bookFile);
			List<String> neededFiles = new ArrayList<String>();
			for (Page page : result.getContent()) {
				neededFiles.add(page.getImage());
				if (page.getMedia() != null && page.getMedia().length() > 0) {
					neededFiles.add(page.getMedia());
				}
			}

			// Open the ZIP file
			in = new ZipInputStream(new FileInputStream(bookFile));

            File dir = new File(context.getFilesDir(), bookFile.getName());
			result.setPath(dir.getAbsolutePath());
			dir.mkdirs();
			ZipEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				if (neededFiles.contains(entry.getName())) {
					String entryName = entry.getName();
					OutputStream out = null;
					try{
						out = new FileOutputStream(new File(dir, entryName));
						Utils.copyStream(in, out);
						out.close();
					} catch (Exception e) {
						Log.e("BooksLoader", "copy problems", e);
					} finally{
						Utils.safeClose(out);
					}
				}
			}
		} catch (IOException e) {
			error(context, R.string.load_error, e);
		} finally {
			Utils.safeClose(in);
		}
		return result;
	}

	public static void loadBookPagesData(Context context, Book book) {
		for (Page page : book.getContent()) {
			loadPageCover(context, book, page);
		}
	}

	public static void loadPageCover(Context context, Book book, Page page) {
		if(page.getImage()==null || page.getImage().length()==0){
			return;
		}
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(book.getPath() + "/" + page.getImage());
			page.setCover(getCoverBitmap(context, fis, page.getImageWidth(), page.getImageHeight()));
		} catch (Exception e) {
			Log.e(LOG_TAG, "unable to make page thumbnail", e);
		} finally {
            Utils.safeClose(fis);
		}
	}

	private static Book loadBookData(File bookFile) throws IOException {
		ZipInputStream in = null;
		Book result = null;
		try {
            Log.i(LOG_TAG, "loadBookData: " + bookFile + " " + bookFile.exists());
            FileInputStream fis = new FileInputStream(bookFile);
			// Open the ZIP file
			in = new ZipInputStream(fis);
			ZipEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				if ("data.xml".equals(entry.getName())) {
					try {
						DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
						DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
						Document doc = documentBuilder.parse(new InputSource(in));
						Element rootElement = doc.getDocumentElement();
						result = Book.fromXml(rootElement);
					} catch (Exception ex) {
						Log.e("BooksLoader", "loadBook", ex);
					}
                    break;
				}
			}
		} finally {
            Utils.safeClose(in);
		}
		return result;
	}

	public static void saveBook(Context context, Book book, String fileName) {
		File f = createFile(context, fileName);
		saveBook(context, book, f);
	}
	
	public static void deleteBook(Context context, BookCover book) {
		new File(book.getBookPath()).delete();
	}

	public static void renameBook(final Activity activity, final BookCover bookCover, final String bookName) {
		final ProgressDialog dialog = ProgressDialog.show(activity, "", activity.getString(R.string.saving_message),
				true);
		new Thread() {
			@Override
			public void run() {
				Book book = loadBook(activity, bookCover);
				book.setName(bookName);
				File f = new File(bookCover.getBookPath());
				if (f.exists()) {
					saveBook(activity, book, f);
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(activity, R.string.save_success, Toast.LENGTH_LONG).show();
						}
					});
				} else {
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							error(activity, R.string.file_not_exist);
						}
					});
				}
				dialog.dismiss();
			};
		}.start();
	}

	public static void saveBookChanges(final Activity activity, final Book book, final String filePath) {
		final ProgressDialog dialog = ProgressDialog.show(activity, "", activity.getString(R.string.saving_message),
				true);
		new Thread() {
			@Override
			public void run() {
				File f = new File(filePath);
				if (f.exists()) {
					saveBook(activity, book, f);
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(activity, R.string.save_success, Toast.LENGTH_LONG).show();
						}
					});
				} else {
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							error(activity, R.string.file_not_exist);
						}
					});
				}
				dialog.dismiss();
			};
		}.start();
	}

	public static void cleanBookData(Context context, Book book) {
		File bookDir = new File(book.getPath());
		if (bookDir.exists() && bookDir.isDirectory()) {
			if (bookDir.listFiles() != null) {
				for (File f : bookDir.listFiles()) {
					f.delete();
				}
			}
			bookDir.delete();
		}
	}

	private static void saveBook(Context context, Book book, File f) {
		if (f != null) {
			// Create a buffer for reading the files
			try {
				// Create the ZIP file
				ZipOutputStream out = new ZipOutputStream(new FileOutputStream(f));
				ByteArrayInputStream in = new ByteArrayInputStream(book.toXml().getBytes("UTF-8"));
				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry("data.xml"));
				Utils.copyStream(in, out);
				// Complete the entry
				out.closeEntry();
				in.close();

				for (Page page : book.getContent()) {
					FileInputStream fis = new FileInputStream(book.getPath() + "/" + page.getImage());
					// Add ZIP entry to output stream.
					out.putNextEntry(new ZipEntry(page.getImage()));
					Utils.copyStream(fis, out);
					out.closeEntry();
					fis.close();
					if (page.getMedia() != null && page.getMedia().length() > 0) {
						fis = new FileInputStream(book.getPath() + "/" + page.getMedia());
						// Add ZIP entry to output stream.
						out.putNextEntry(new ZipEntry(page.getMedia()));
						Utils.copyStream(fis, out);
						out.closeEntry();
						fis.close();
					}
				}
				// Complete the ZIP file
				out.close();
				if (saveHandler != null) {
					saveHandler.onSaved(f, book);
					saveHandler = null;
				}
				coversCache = null;
			} catch (IOException e) {
				Log.e("BooksLoader", "saveBook", e);
			}
		}
	}

	private static File createFile(Context context, String fileName) {
		File appDir = Utils.getAppDir(context);
		File newFile = new File(appDir, fileName);
		if (newFile.exists()) {
			error(context, R.string.file_exist);
			return null;
		}
		try {
			if (newFile.createNewFile()) {
				return newFile;
			} else {
				error(context, R.string.file_creation_error);
				return null;
			}
		} catch (IOException e) {
			error(context, R.string.file_creation_error, e);
			return null;
		}
	}

	private static void error(Context context, int messageId) {
		Log.e(LOG_TAG, context.getString(messageId));
		Toast.makeText(context, messageId, Toast.LENGTH_LONG).show();
	}

	private static void error(Context context, int messageId, Exception ex) {
		Log.e(LOG_TAG, context.getString(messageId), ex);
		Toast.makeText(context, messageId, Toast.LENGTH_LONG).show();
	}

	private static void findFiles(File[] files, List<String> names) {
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					findFiles(files[i].listFiles(), names);
				}
				names.add(files[i].getAbsolutePath());
			}
		}
	}
}
