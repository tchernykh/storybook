ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* default.properties
* lib\
* lib\.svn\
* lib\.svn\all-wcprops
* lib\.svn\entries
* lib\.svn\prop-base\
* lib\.svn\prop-base\apache-mime4j-0.6.jar.svn-base
* lib\.svn\prop-base\httpclient-4.0.3.jar.svn-base
* lib\.svn\prop-base\httpcore-4.0.1.jar.svn-base
* lib\.svn\prop-base\httpmime-4.0.3.jar.svn-base
* lib\.svn\text-base\
* lib\.svn\text-base\apache-mime4j-0.6.jar.svn-base
* lib\.svn\text-base\httpclient-4.0.3.jar.svn-base
* lib\.svn\text-base\httpcore-4.0.1.jar.svn-base
* lib\.svn\text-base\httpmime-4.0.3.jar.svn-base
* lib\apache-mime4j-0.6.jar
* lib\httpclient-4.0.3.jar
* lib\httpcore-4.0.1.jar
* lib\httpmime-4.0.3.jar

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets\
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
