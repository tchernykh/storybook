### What is StoryBook? ###
This app allows you to create a book from images that you have in your gallery, record a voice note to these images and share your books with your friends.

Current version: 1.0: https://play.google.com/store/apps/details?id=com.yabby.storybook

### How do I get set up? ###
Just import this project to Android Studio.